//
//  CheckBoxCellView.swift
//  Subler
//
//  Created by Damiano Galassi on 20/10/2017.
//

import Cocoa

class CheckBoxCellView : NSTableCellView {
    @IBOutlet var checkboxButton: NSButton!
}
