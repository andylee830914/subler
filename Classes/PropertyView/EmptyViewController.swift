//
//  EmptyViewController.swift
//  Subler
//
//  Created by Damiano Galassi on 20/10/2017.
//

import Cocoa

class EmptyViewController: NSViewController {

    override var nibName: NSNib.Name? {
        return NSNib.Name(rawValue: "EmptyView")
    }

}
